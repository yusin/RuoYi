package com.ruoyi.system.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.ruoyi.system.domain.SysDept;
import com.ruoyi.system.domain.User;
import java.util.List;

public interface UserMapper  {
    /**
     * 查询部门管理数据
     *
     * @param user 部门信息
     * @return 部门信息集合
     */
    public List<User> selectUserList(User user);


    /**
     * 获取用户信息
     * @param userId
     * @return
     */
    public User selectUserByUserId(Integer userId);

}
