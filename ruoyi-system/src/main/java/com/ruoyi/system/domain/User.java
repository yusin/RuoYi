package com.ruoyi.system.domain;

import com.ruoyi.common.core.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Data
@Getter
@Setter
/** 表user的实体类 */
public class User extends BaseEntity
{

    /** id */
    private Integer userId;
    /** 会员用户名 */
    private String username;
    /** 手机号码 */
    private String phoneNumber;
    /** 用户密码 md5加密 */
    private String password;
    /** 用户余额 */
    private double balance;
    /** 昵称 */
    private String nickname;
    /** 真实姓名 */
    private String realname;
    /** 性别(0 未知 1 男  2女) */
    private short sex;
    /** 生日 */
    private Date birthday;
    /** 身份证号码 */
    private String identityCard;
    /** 邮箱 */
    private String email;
    /** 学校 */
    private String school;
    /** 爱好(多个以英文逗号分隔) */
    private String hobby;
    /** 职业名称 */
    private String job;
    /** 身高(单位:cm) */
    private Integer height;
    /** 体重(单位:kg) */
    private Integer weight;
    /** 职业名称 */
    private String professionName;
    /** 行业 */
    private Integer industry;
    /** 行业名称 */
    private String industryName;
    /** 年纪区间 */
    private Integer age;
    /** 年龄范围名称 */
    private String ageName;
    /** 学历 */
    private Integer education;
    /** 学历名称 */
    private String educationName;
    /** 省份 */
    private String province;
    /** 省级区域编码 */
    private String provinceCode;
    /** 城市 */
    private String city;
    /** 市级区域编码 */
    private String cityCode;
    /** 头像图片 */
    private String avatar;
    /** 审核状态(0 待申请 1 待支付 2 已支付待审核 3 审核成功 4 驳回申请 5 已绑定设备 ) */
    private short auditStatus;
    /** 设备申请时间 */
    private Date applyTime;
    /** 审核设备时间 */
    private Date auditTime;
    /** 申请手机型号 */
    private Integer deviceCategoryId;
    /** 邀请码 */
    private String inviteCode;
    /** 收货手机号 */
    private String deliveryPhone;
    /** 收货人名称 */
    private String deliveryName;
    /** 收货地址 */
    private String deliveryAddress;
    /** 微信OpenID */
    private String wxOpenId;
    /** 微信UnionID */
    private String wxUnionId;
    /** 支付宝UserID */
    private String alipayUserId;
    /** 注册Ip地址 */
    private String registerIp;
    /** 个人简介 */
    private String remark;
    /** 设备激活时间 */
    private Date activeTime;
    /** 是否启用 */
    private boolean isEnable;
    /** 创建时间 */
    private Date createTime;
    /** 后台审批人ID */
    private Integer auditUserId;
    /** 后台审批人名称 */
    private String auditUserName;
    /** 设备指定(分配)人ID */
    private Integer assignUserId;
    /** 设备指定(分配)人名称 */
    private String assignUserName;
    /** 设备指定(分配)时间 */
    private Date assignTime;
    /** 用户金币余额 */
    private Integer coin;
    /** 任务手机手机号码 */
    private String taskPhoneNumber;
    /** 区(县)编码 */
    private String areaCode;
    /** 区(县) */
    private String area;
    /** 收货人配送地区信息 */
    private String deliveryArea;
    /** 审核意见(驳回原因) */
    private String auditOpinions;

}