package com.ruoyi.system.domain;

//import com.baomidou.mybatisplus.annotation.TableId;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 会员账号(第三方)表 account
 * 
 * @author ruoyi
 * @date 2019-06-12
 */

public class Account extends BaseEntity
{
	private static final long serialVersionUID = 1L;
	
	/**  */
//	@TableId
	private Integer accountId;
	/** 账号所属用户ID */
	private Integer userId;
	/** 账号平台类型(1 微信 2 QQ 4 微博 8 淘宝 16 京东) */
	private Integer platform;
	/** 账号登录名 */
	private String username;
	/** 用户密码 */
	private String password;
	/** 账号状态(0 未验证 1 已验证 2 账号或密码错误) */
	private Integer status;
	/** 删除标志（0代表存在 2代表删除） */
	private Integer delFlag;

	public void setAccountId(Integer accountId) 
	{
		this.accountId = accountId;
	}

	public Integer getAccountId() 
	{
		return accountId;
	}
	public void setUserId(Integer userId) 
	{
		this.userId = userId;
	}

	public Integer getUserId() 
	{
		return userId;
	}
	public void setPlatform(Integer platform) 
	{
		this.platform = platform;
	}

	public Integer getPlatform() 
	{
		return platform;
	}
	public void setUsername(String username) 
	{
		this.username = username;
	}

	public String getUsername() 
	{
		return username;
	}
	public void setPassword(String password) 
	{
		this.password = password;
	}

	public String getPassword() 
	{
		return password;
	}
	public void setStatus(Integer state)
	{
		this.status = state;
	}

	public Integer getStatus()
	{
		return status;
	}
	public void setDelFlag(Integer delFlag) 
	{
		this.delFlag = delFlag;
	}

	public Integer getDelFlag() 
	{
		return delFlag;
	}

    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("accountId", getAccountId())
            .append("userId", getUserId())
            .append("platform", getPlatform())
            .append("username", getUsername())
            .append("password", getPassword())
            .append("state", getStatus())
//            .append("createTime", getCreateTime())
//            .append("updateTime", getUpdateTime())
            .append("delFlag", getDelFlag())
//            .append("remark", getRemark())
            .toString();
    }
}
