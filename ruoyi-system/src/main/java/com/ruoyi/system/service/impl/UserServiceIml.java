package com.ruoyi.system.service.impl;

import com.ruoyi.system.domain.User;
import com.ruoyi.system.mapper.SysUserMapper;
import com.ruoyi.system.mapper.UserMapper;
import com.ruoyi.system.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 用户 业务层 实现
 */
@Service
public class UserServiceIml implements IUserService {


    @Autowired
    private UserMapper userMapper;

    /**
     * 获取用户信息
     * @param user 用户信息
     * @return
     */
    @Override
    public List<User> selectUserList(User user) {
        return userMapper.selectUserList(user);
    }

    /**
     * 根据Id获取用户信息
     * @param userId
     * @return
     */
    @Override
    public User selectUserByUserId(Integer userId) {
        return userMapper.selectUserByUserId(userId);
    }


}
