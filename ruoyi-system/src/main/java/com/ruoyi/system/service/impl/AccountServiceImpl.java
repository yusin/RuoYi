package com.ruoyi.system.service.impl;

import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.AccountMapper;
import com.ruoyi.system.domain.Account;
import com.ruoyi.system.service.IAccountService;
import com.ruoyi.common.core.text.Convert;

/**
 * 会员账号(第三方) 服务层实现
 *
 * @author ruoyi
 * @date 2019-06-12
 */
@Service
public class AccountServiceImpl implements IAccountService
{
	@Autowired
	private AccountMapper accountMapper;

	/**
	 * 查询会员账号(第三方)信息
	 *
	 * @param accountId 会员账号(第三方)ID
	 * @return 会员账号(第三方)信息
	 */
	@Override
	public Account selectAccountById(Integer accountId)
	{
		return accountMapper.selectAccountById(accountId);
	}

	/**
	 * 查询会员账号(第三方)列表
	 *
	 * @param account 会员账号(第三方)信息
	 * @return 会员账号(第三方)集合
	 */
	@Override
	public List<Account> selectAccountList(Account account)
	{
		return accountMapper.selectAccountList(account);
	}

	/**
	 * 新增会员账号(第三方)
	 *
	 * @param account 会员账号(第三方)信息
	 * @return 结果
	 */
	@Override
	public int insertAccount(Account account)
	{
		return accountMapper.insertAccount(account);
	}

	/**
	 * 修改会员账号(第三方)
	 *
	 * @param account 会员账号(第三方)信息
	 * @return 结果
	 */
	@Override
	public int updateAccount(Account account)
	{
		account.setUpdateTime(new Date());
		return accountMapper.updateAccount(account);
	}

	/**
	 * 删除会员账号(第三方)对象
	 *
	 * @param ids 需要删除的数据ID
	 * @return 结果
	 */
	@Override
	public int deleteAccountByIds(String ids)
	{
		return accountMapper.deleteAccountByIds(Convert.toStrArray(ids));
	}

}
