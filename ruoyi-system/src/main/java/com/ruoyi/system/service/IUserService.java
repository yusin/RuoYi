package com.ruoyi.system.service;

import com.ruoyi.system.domain.User;
import org.apache.commons.fileupload.util.LimitedInputStream;
import org.omg.PortableInterceptor.USER_EXCEPTION;
import org.omg.PortableServer.LIFESPAN_POLICY_ID;

import java.util.List;

/**
 *  * 用户 业务层 抽象
 */
public interface IUserService {

    /**
     * 根据条件分页查询用户列表
     *
     * @param user 用户信息
     * @return 用户信息集合信息
     */
    public List<User> selectUserList(User user);

    /**
     * 根据Id获取用户信息
     * @param userId
     * @return
     */
    public User selectUserByUserId(Integer userId);

}
