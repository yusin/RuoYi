package com.ruoyi.system.service;

import com.ruoyi.system.domain.Account;
import java.util.List;

/**
 * 会员账号(第三方) 服务层
 * 
 * @author ruoyi
 * @date 2019-06-12
 */
public interface IAccountService 
{
	/**
     * 查询会员账号(第三方)信息
     * 
     * @param accountId 会员账号(第三方)ID
     * @return 会员账号(第三方)信息
     */
	public Account selectAccountById(Integer accountId);
	
	/**
     * 查询会员账号(第三方)列表
     * 
     * @param account 会员账号(第三方)信息
     * @return 会员账号(第三方)集合
     */
	public List<Account> selectAccountList(Account account);
	
	/**
     * 新增会员账号(第三方)
     * 
     * @param account 会员账号(第三方)信息
     * @return 结果
     */
	public int insertAccount(Account account);
	
	/**
     * 修改会员账号(第三方)
     * 
     * @param account 会员账号(第三方)信息
     * @return 结果
     */
	public int updateAccount(Account account);
		
	/**
     * 删除会员账号(第三方)信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	public int deleteAccountByIds(String ids);
	
}
