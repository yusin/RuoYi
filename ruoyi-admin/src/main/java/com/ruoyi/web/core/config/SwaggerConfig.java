package com.ruoyi.web.core.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import com.ruoyi.common.config.Global;
import io.swagger.annotations.ApiOperation;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Swagger2的接口配置
 * 
 * @author ruoyi
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig
{
    /**
     * 创建API
     */
    @Bean
    public Docket createRestApi()
    {
        return new Docket(DocumentationType.SWAGGER_2)
                // 用来创建该API的基本信息，展示在文档的页面中（自定义展示的信息）
                .apiInfo(apiInfo())
                // 设置哪些接口暴露给Swagger展示
                .select()
                // 扫描所有有注解的api，用这种方式更灵活
                .apis(RequestHandlerSelectors.withMethodAnnotation(ApiOperation.class))
                // 扫描指定包中的swagger注解
                //.apis(RequestHandlerSelectors.basePackage("com.ruoyi.project.tool.swagger"))
                // 扫描所有
                .apis(RequestHandlerSelectors.any())
                .paths(PathSelectors.any())
                .build();
    }

    /**
     * 添加摘要信息
     */
    private ApiInfo apiInfo()
    {
        StringBuilder sbDesc = new StringBuilder();
        sbDesc.append("接口说明");
        sbDesc.append("\n");
        sbDesc.append("0.身份验证传参\n Authorization=Bearer {token}");
        sbDesc.append("\n");
        sbDesc.append("1.状态码说明\n 所有成功操作返回状态码为200,如果返回非200状态码表示操作失败，需要打印Message中的信息，未授权访问返回401");
        sbDesc.append("\n");
        sbDesc.append("2.极光推送 AppKey:7315fed979c21162d2c2f282");
        sbDesc.append("\n");
        sbDesc.append("3.测试Token \n 15512345678 eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjEwMDAwNCIsInBob25lIjoiMTU1MTIzNDU2NzgiLCJuYW1lIjoic3RyaW5nIiwibmJmIjoxNTQ4MzE2ODQ2LCJleHAiOjE1NDk1MjY0NDYsImlzcyI6Imh0dHA6Ly9hcGkueXVua29uZ3NqLmNvbSIsImF1ZCI6ImFwaSJ9.5cBR9HdMzy-ZCw7K5OPtI6KNlWayURI79Hh4TSetuNY");
        sbDesc.append("\n18007300992 eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjEwMDAwNyIsIm5hbWUiOiIxODAwNzMwMDk5MiIsIm5iZiI6MTU0MjA5MzU0MiwiZXhwIjoxNTQzMzAzMTQyLCJpc3MiOiJodHRwOi8vYXBpLnl1bmtvbmdzai5jb20iLCJhdWQiOiJhcGkifQ.M_a5wYC2o_L19cRTSM-bAFVu6L7Xm-UDUqIb8MoZakc");
        sbDesc.append("\n");
        sbDesc.append("4.云控宝APP服务协议 \n http://api.yunkongsj.com/html/agreement.html");
        // 用ApiInfoBuilder进行定制
        return new ApiInfoBuilder()
                // 设置标题
                .title("云控宝系统_接口文档")
                // 描述
                .description(sbDesc.toString())
                // 作者信息
                .contact(new Contact(Global.getName(), null, null))
                // 版本
                .version("版本号:" + Global.getVersion())
                .build();
    }
}
