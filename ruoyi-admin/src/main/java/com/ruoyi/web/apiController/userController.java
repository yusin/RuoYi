package com.ruoyi.web.apiController;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.ruoyi.common.config.ServerConfig;
import com.ruoyi.system.domain.User;
import com.ruoyi.system.service.IAccountService;
import com.ruoyi.system.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.StringUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;

@Api("用户信息管理")
@RestController
@RequestMapping("/api/user")
public class userController  extends BaseController{


    @Autowired
    private IUserService userService;

    @ApiOperation("获取用户列表")
    @GetMapping("/list")
    public AjaxResult userList() {
//        List<UserEntity2> userList = new ArrayList<UserEntity2>(users.values()); new User()
        List<User> userList=userService.selectUserList(null);
        return AjaxResult.success(userList);
    }

    @ApiOperation("获取用户详细")
    @ApiImplicitParam(name = "userId", value = "用户ID", required = true, dataType = "int", paramType = "path")
    @GetMapping("/{userId}")
    public AjaxResult getUser(@PathVariable Integer userId) {
        User user=userService.selectUserByUserId(userId);
        if(user==null){
            return error("用户不存在");
        }
        return AjaxResult.success(user);
    }

    @ApiOperation("更新用户")
    @ApiImplicitParam(name = "userEntity", value = "新增用户信息", dataType = "UserEntity")
    @PutMapping("/update")
    public AjaxResult update(User user) {
        if (StringUtils.isNull(user) || StringUtils.isNull(user.getUpdateTime())) {
            return error("用户ID不能为空");
        }
//        if (users.isEmpty() || !users.containsKey(user)) {
//            return error("用户不存在");
//        }
        return AjaxResult.success();
    }
}
