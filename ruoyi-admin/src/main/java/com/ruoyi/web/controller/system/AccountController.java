package com.ruoyi.web.controller.system;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.Account;
import com.ruoyi.system.service.IAccountService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;

/**
 * 会员账号(第三方) 信息操作处理
 * 
 * @author ruoyi
 * @date 2019-06-12
 */
@Controller
@RequestMapping("/system/account")
public class AccountController extends BaseController
{
    private String prefix = "system/account";
	
	@Autowired
	private IAccountService accountService;
	
	@RequiresPermissions("system:account:view")
	@GetMapping()
	public String account()
	{
	    return prefix + "/account";
	}
	
	/**
	 * 查询会员账号(第三方)列表
	 */
	@RequiresPermissions("system:account:list")
	@PostMapping("/list")
	@ResponseBody
	public TableDataInfo list(Account account)
	{
		startPage();
        List<Account> list = accountService.selectAccountList(account);
		return getDataTable(list);
	}
	
	
	/**
	 * 导出会员账号(第三方)列表
	 */
	@RequiresPermissions("system:account:export")
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(Account account)
    {
    	List<Account> list = accountService.selectAccountList(account);
        ExcelUtil<Account> util = new ExcelUtil<Account>(Account.class);
        return util.exportExcel(list, "account");
    }
	
	/**
	 * 新增会员账号(第三方)
	 */
	@GetMapping("/add")
	public String add()
	{
	    return prefix + "/add";
	}
	
	/**
	 * 新增保存会员账号(第三方)
	 */
	@RequiresPermissions("system:account:add")
	@Log(title = "会员账号(第三方)", businessType = BusinessType.INSERT)
	@PostMapping("/add")
	@ResponseBody
	public AjaxResult addSave(Account account)
	{		
		return toAjax(accountService.insertAccount(account));
	}

	/**
	 * 修改会员账号(第三方)
	 */
	@GetMapping("/edit/{accountId}")
	public String edit(@PathVariable("accountId") Integer accountId, ModelMap mmap)
	{
		Account account = accountService.selectAccountById(accountId);
		mmap.put("account", account);
	    return prefix + "/edit";
	}
	
	/**
	 * 修改保存会员账号(第三方)
	 */
	@RequiresPermissions("system:account:edit")
	@Log(title = "会员账号(第三方)", businessType = BusinessType.UPDATE)
	@PostMapping("/edit")
	@ResponseBody
	public AjaxResult editSave(Account account)
	{		
		return toAjax(accountService.updateAccount(account));
	}
	
	/**
	 * 删除会员账号(第三方)
	 */
	@RequiresPermissions("system:account:remove")
	@Log(title = "会员账号(第三方)", businessType = BusinessType.DELETE)
	@PostMapping( "/remove")
	@ResponseBody
	public AjaxResult remove(String ids)
	{		
		return toAjax(accountService.deleteAccountByIds(ids));
	}
	
}
